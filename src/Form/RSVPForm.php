<?php

namespace Drupal\rsvplist\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Component\Utility\EmailValidator;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserStorageInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an RSVP Email form.
 */
class RSVPForm extends FormBase {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * Email Validator.
   *
   * @var Drupal\Component\Utility\EmailValidator
   */
  protected $emailValidator;

  /**
   * The current account.
   *
   * @var Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The user account.
   *
   * @var Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The database connection.
   *
   * @var Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The messenger.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Class Constructor.
   */
  public function __construct(CurrentRouteMatch $route_match, EmailValidator $email_validator, AccountProxyInterface $account, UserStorageInterface $userStorage, Connection $database, MessengerInterface $messenger) {
    $this->routeMatch = $route_match;
    $this->emailValidator = $email_validator;
    $this->account = $account;
    $this->userStorage = $userStorage;
    $this->database = $database;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('current_route_match'),
          $container->get('email.validator'),
          $container->get('current_user'),
          $container->get('entity_type.manager')->getStorage('user'),
          $container->get('database'),
          $container->get('messenger'),
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rsvplist_email_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node = $this->routeMatch->getParameter('node');
    if (isset($node)) {
      $nid = $node->id();
    }

    $form['email'] = [
      '#title' => $this->t('Email Address'),
      '#type' => 'textfield',
      '#size' => 25,
      '#description' =>
      $this->t("We'll send updates to the email address your provide."),
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('RSVP'),
    ];
    $form['nid'] = [
      '#type' => 'hidden',
      '#value' => $nid,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValue('email');
    if ($value == !$this->emailValidator->isValid($value)) {
      $form_state->setErrorByName('email', $this->t(
            'The email address %mail is not valid.',
            ['%mail' => $value]
        ));
      return;
    }
    $node = $this->routeMatch->getParameter('node');
    // Check if email already is set for this node.
    $select = Database::getConnection()->select('rsvplist', 'r');
    $select->fields('r', ['nid']);
    $select->condition('nid', $node->id());
    $select->condition('mail', $value);
    $results = $select->execute();

    if (!empty($results->fetchCol())) {
      // We found a row with this nid and email.
      $form_state->setErrorByName('email', $this->t('The address %mail is already subscribed to this list.', ['%mail' => $value]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = $this->userStorage->load($this->account->id());
    $this->database->insert('rsvplist')
      ->fields([
        'mail' => $form_state->getValue('email'),
        'nid' => $form_state->getValue('nid'),
        'uid' => $user->id(),
        'created' => time(),
      ])
      ->execute();
    $this->messenger->addMessage($this->t('Thank you for your RSVP, you are on the list for the event.'));
  }

}
